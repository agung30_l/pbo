/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author AgungJa30
 */
public class LoopingLogikaIF {
    public static void main(String[] args){
        for(int i=1; i<=10; i++){
            if(i % 2 ==0){
                System.out.println("Angka "+i+" adalah Genap");
            }else{
                System.out.println("Angka "+i+" adalah Ganjil");
            }  
        }
    }  
}

