/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */

/**
 *
 * @author AgungJa30
 */
public class LatihanLooping {
    public static void main(String[] args) {
        for(int i=1;i<=10;i+=2){
            System.out.println(i);
        }
        System.out.println("=======");
        for(int i=10;i>=1;i--){
            System.out.println(i);
        }
    }
}

