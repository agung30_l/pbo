import java.util.Scanner;
public class LogikaIF {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Silahkan Masukkan Nilai : ");
        int nilai = sc.nextInt();
        if(nilai>=85 && nilai<=100){
            System.out.println("Grade A");
        }else if(nilai>=70 && nilai<=80){
            System.out.println("Grade B");
        }else if(nilai>=60 && nilai<=70){
            System.out.println("Grade C");
        }else if(nilai>=50 && nilai<=60){
            System.out.println("Grade D");
        }else{
            System.out.println("Maaf, Grade Tidak Ada");
        }
    }
}
