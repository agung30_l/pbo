package Controller;
import java.sql.*;
import javax.swing.JOptionPane;
import javax.swing.table.DefaultTableModel;
import Model.Prodi;
import javax.swing.table.TableModel;

public class ProdiController {
    
    Connection conn = null;
    PreparedStatement stmt;
    Prodi prodi = new Prodi();
    DefaultTableModel tb;
    ResultSet rs;
    
    public void tambahProdi(String nama){
        String sql = "INSERT INTO prodi SET nama = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setString(1, nama);
            stmt.executeUpdate();
            JOptionPane.showMessageDialog(null,"Data Berhasil Ditambah");
        }catch(Exception e){
            JOptionPane.showMessageDialog(null,"Data Gagal Ditambah");
        }
    }
    
    public DefaultTableModel viewProdi(){
        Object header[] = {"ID", "Prodi",};
        tb = new DefaultTableModel(null, header);
        String sql = "SELECT * FROM prodi ";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            rs = stmt.executeQuery();
            while(rs.next()){
                String kolom1 = rs.getString(1);
                String kolom2 = rs.getString(2);
                String kolom[] = {kolom1,kolom2};
                tb.addRow(kolom);
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
        return tb;
    }
    
     public void updateProdi(int id, String nama){
        String sql = "UPDATE prodi SET nama = ? WHERE id = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setInt(2, id);
            stmt.setString(1, nama);
            int hasil = stmt.executeUpdate();
            if(hasil > 0){
                JOptionPane.showMessageDialog(null, "Data Prodi "
                        + "Berhasil Diperbaharui");
            }else{
                JOptionPane.showMessageDialog(null, "Data Prodi "
                        + "Gagal Diperbaharui");
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
    }
    
    public void deleteProdi(int id){
        String sql = "DELETE FROM prodi WHERE id = ?";
        try{
            conn = Koneksi.getConnection();
            stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            int hasil = stmt.executeUpdate();
            if(hasil > 0){
                JOptionPane.showMessageDialog(null, "Data Prodi "
                        + "Berhasil Dihapus");
            }else{
                JOptionPane.showMessageDialog(null, "Data Prodi "
                        + "Gagal Dihapus");
            }
        }catch(Exception e){
            JOptionPane.showMessageDialog(null, "Koneksi Gagal");
        }
    }

    public TableModel viewprodi() {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }

    public void updateprodi(int id, String nama) {
        throw new UnsupportedOperationException("Not supported yet."); // Generated from nbfs://nbhost/SystemFileSystem/Templates/Classes/Code/GeneratedMethodBody
    }
    
}